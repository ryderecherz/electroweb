# -*- coding: utf-8 -*-
from database import Database


class Product:
    def __init__(self, product):
        self._name = product.get('name', False)
        self._code = product.get('code', False)
        self._measure_id = product.get('measure_id', False)


    @staticmethod
    def browse(product_id):
        browse_product_query = """select * from product where id = {product_id}""".format(product_id=product_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchone()
        return product

    @staticmethod
    def list_product():
        browse_product_query = """select * from product"""
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchall()
        return product

    @staticmethod
    def browsebymeasure(measure_id):
        browse_product_query = """select * from product where measure_id = {measure_id}""".format(measure_id=measure_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchall()
        return product

    def create_product(self):
        create_product_query = """
            insert into product(name, code,measure_id) VALUES ('{name}','{code}',{measure_id})
        """.format(name=self.name, code=self.code, measure_id=self.measure_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(create_product_query)
        ps_connection.commit()
        ps_cursor.close()
        ps_connection.close()


    @property
    def name(self):
        return self._name

    @property
    def code(self):
        return self._code

    @property
    def measure_id(self):
        return self._measure_id

    @name.setter
    def name(self, name):
        self._name = name

    @code.setter
    def code(self, code):
        self._code = code

    @measure_id.setter
    def measure_id(self, measure_id):
        self._measure_id = measure_id

    def __repr__(self):
        return "Product" % self._name
